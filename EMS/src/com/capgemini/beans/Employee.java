package com.capgemini.beans;

public class Employee 
{

	
	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Employee(String name, int id) {
		super();
		this.name = name;
		this.id = id;
	}
	public Employee(String name, int id, String emailid, double salary) {
		super();
		this.name = name;
		this.id = id;
		this.emailid = emailid;
		this.salary = salary;
	}
	String name;
	int id;
	String emailid;
	double salary;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "Employee [name=" + name + ", id=" + id + ", emailid=" + emailid + ", salary=" + salary + "]";
	}
	
	
}
