package com.capgemini.dao;
import java.util.ArrayList;

import com.capgemini.beans.*;
import com.capgemini.exceptions.*;
public interface EmployeeDAO 
{

	
	public int addEmployee(Employee employee)throws EMSEXCEPTION;
	public Employee viewEmployee(int id)throws EMSEXCEPTION;
	public Employee removeEmployee(int id)throws EMSEXCEPTION;
	public void updateEmployee(int id,Employee employee)throws EMSEXCEPTION;
	public ArrayList<Employee> getAllEmployee()throws EMSEXCEPTION;
	
	
	
}
