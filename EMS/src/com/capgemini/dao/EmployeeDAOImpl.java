package com.capgemini.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import com.capgemini.beans.*;
import com.capgemini.exceptions.*;
import java.util.ArrayList;

import com.capgemini.beans.Employee;
import com.capgemini.exceptions.EMSEXCEPTION;

public class EmployeeDAOImpl implements EmployeeDAO {
  
	ArrayList<Employee> list=new ArrayList<Employee>();
	public int generateId()
	{
		
		Random rand=new Random();
		int x=rand.nextInt(1000);
		return x;
		
		
	}
	
	@Override
	public int addEmployee(Employee employee) throws EMSEXCEPTION 
	{
	int y=generateId();
	employee.setId(y);
	list.add(employee);
	return y;
		
	}

	@Override
	public Employee viewEmployee(int id) throws EMSEXCEPTION 
	{
		Employee p=null;
		int x=9;
		Iterator it=list.iterator();
		while(it.hasNext())
		{
			
		
		p=(Employee)it.next();
		if(p.getId()==id)
		{
			x++;	
			return p;
		     
		}
		}	
			return null;

	}

	@Override
	public Employee removeEmployee(int id) throws EMSEXCEPTION 
	{
		
		int op=1;
		
		Iterator it=list.iterator();
		while(it.hasNext())
		{
			
		Employee p;
		p=(Employee)it.next();
		if(p.getId()==id)
		{
		
		list.remove(p);
		op++;
		return p;
		}
		}
		
		return null;
		
	}

	@Override
	public void updateEmployee(int id,Employee employee) throws EMSEXCEPTION
	{
		Iterator it=list.iterator();
		while(it.hasNext())
		{
			
		Employee p;
		p=(Employee)it.next();
		if(p.getId()==id)
		{
		
		list.remove(p);
		list.add(employee);    
		}
		}
		
	}

	@Override
	public ArrayList<Employee> getAllEmployee() throws EMSEXCEPTION 
	{
		
		return list;
	}

}
