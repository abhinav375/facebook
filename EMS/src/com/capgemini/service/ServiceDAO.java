package com.capgemini.service;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import com.capgemini.beans.*;
import com.capgemini.exceptions.*;
import java.util.ArrayList;

import com.capgemini.beans.Employee;
import com.capgemini.exceptions.EMSEXCEPTION;
import com.capgemini.dao.*;
public interface ServiceDAO 
{

	public int addEmployee(Employee employee)throws EMSEXCEPTION;
	public Employee viewEmployee(int id)throws EMSEXCEPTION;
	public Employee removeEmployee(int id)throws EMSEXCEPTION;
	public void updateEmployee(int id,Employee employee)throws EMSEXCEPTION;
	public ArrayList<Employee> getAllEmployee()throws EMSEXCEPTION;
	public boolean isEmployeeValid(Employee employee);
	
	
	
	
	
	
	
	
	
}
