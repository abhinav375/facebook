package com.capgemini.service;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.capgemini.beans.Employee;
import com.capgemini.exceptions.EMSEXCEPTION;
import com.capgemini.dao.*;
public class ServiceDAOImpl implements ServiceDAO 
{
	EmployeeDAO obj=new EmployeeDAOImpl();

	@Override
	public int addEmployee(Employee employee) throws EMSEXCEPTION 
	{
		if( isEmployeeValid(employee))
		{
			
			int y=obj.addEmployee(employee);
			return y;
		}
		else
			throw new EMSEXCEPTION("email is not write");
		 
	}

	@Override
	public Employee viewEmployee(int id) throws EMSEXCEPTION 
	{
		if( id>0)
		{
			
			Employee y=obj.viewEmployee(id);
			return y;
		}
		else
			throw new EMSEXCEPTION("id cannot be negative");

	}

	@Override
	public Employee removeEmployee(int id) throws EMSEXCEPTION
	{
	
		if( id>0)
		{
			
			Employee y=obj.removeEmployee(id);
			return y;
		}
		else
			throw new EMSEXCEPTION("id cannot be negative");
		
		
		
		
	}

	@Override
	public void updateEmployee(int id, Employee employee) throws EMSEXCEPTION 
	{
		
		
		
		
		if( isEmployeeValid(employee))
		{
			
			obj.updateEmployee(id, employee);
		}
		
		
		else
			throw new EMSEXCEPTION("id cannot be negative");
		

	}

	@Override
	public ArrayList<Employee> getAllEmployee() throws EMSEXCEPTION 
	{
	ArrayList<Employee> poi=obj.getAllEmployee();
	return poi;
	}

	@Override
	public boolean isEmployeeValid(Employee employee) 
	{
		Pattern p=Pattern.compile("[a-zA-Z0-9.]+[a-zA-Z0-9._]*@[a-zA-Z0-9]+([.][a-zA-Z]+)+");
		Matcher m=p.matcher(employee.getEmailid());
		if(m.find()&&m.group().equals(employee.getEmailid()))
		{
			return true;
			
		}
		
		return false;
	}

}
